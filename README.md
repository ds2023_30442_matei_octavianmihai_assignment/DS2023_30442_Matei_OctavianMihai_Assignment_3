# DS2023 30442 Matei Octavian Mihai Assignment 3

## Description of the project

This project is aimed at creating a distributed system with:

- Frontend Project
- User interaction Project
- Product interation Project
- Device consumption Project
- Chat Project
- One database per each backend project
- Standalone device simulation Project

In order to do implement these, we will use the following technologies:

- `React` for frontend
- `Spring` and `Java` for backends
- `PostgresSQL` for the database
- `Docker` and `Docker Desktop` for the deployment
- `Python` for device simulator

The goal is to create a website that allows the users to be split into two, an administrator and multiple clients. Each user has different ways of interacting with our system as follows:

### `Client`

The client can register, login and only see its own devices with their consumption. For each device, the user can check charts per each day of consumption. The client can also `chat` with the administrator.

### `Administrator`

The administrator has more actions such as:

- `CRUD` on all users
- `CRUD` on all devices
- The ability to `pair` users to devices
- The ability to `chat` with the users

<div style="page-break-after: always;"></div>

## Conceptual architecture of the distributed system

![Alt text](Images/ConceptualDiagram.png)
_Fig 1: Conceptual Diagram_

As it can be seen in the image `Fig 1`, The architecture of the project is made up of 4 main elements:

### `The Frontend`

- The frontend is composed of 2 main components.
  - A `CommonUserTable` that acts as the main page for normal users. A user can see their devices, their respective consumptions, a date picker and a button to display the chart. The user also has a `Support` button that opens a chat popup with the administrator.
  - A `UserTable` that acts as a repository of all the users. The admin can communicate with the online users. The chat api has the ability to display if the other user is typing sth showing the text `...is typing`. The api has also the ability to store and display what messages were seen by the other user and display to the current one.

The communication between frontend and backend is done using Websockets with the following properties:

- The connection is done on the url: `http://localhost:8084/ws`
- The sending to the service is done on:
  - login: `/app/authenticate`
  - logout: `/app/logout`
  - fetch_users: `/app/fetch_users`
  - send_private: `/app/private-message`
  - seen_messages: `/app/seen_messages`
  - is_typing: `/app/is_typing`
- The data subscription is done on:
  - login: ``/user/{jwt key}/authenticate_response`
  - logout: ``/user/{jwt key}/authenticate_response`
  - fetch_users: ``/user/{jwt key}/fetch_users_response`
  - send_private: ``/user/{jwt key}/private`
  - seen_messages: ``/user/{jwt key}/seen_messages`
  - is_typing: ``/user/{jwt key}/typing_response`

### `The Backends`

The backends are Spring applications. The main backends for this part of the project are composed of:

1. `User Backend` that creates the JWT token when logging or registering. This will be used by all backends to authorise the users using a secret key. This key is transmitted to all the other backends and a uniform decryption can happen throughout the entire project
2. `Chat Backend` that has controllers and services related to the chat functionality to authorize access to the service, to communicate with websockets to and from users.

## UML Deployment diagram

![Alt text](Images/DeploymentDiagram.png)
_Fig 2: Deployment Diagram_

As it can be seen in `Fig 2`, the deployment is made in Docker having each component of the project in a separate `container`. This allows for greater customizability for each. The main difference in the implementation is that during deployment and actual run of the project, the urls are changed from `localhost` to the one that the component interacts with and the ports are changed according to the one that is mapped in the file. For example:

- For the product backend to access their database, we change:

  - from `localhost:5432\user_db` to `userpostgres\product_db` which is on port `1001`
  - from `localhost:5432\product_db` to `productpostgres\product_db` which is on port `1002`
  - from `localhost:5432\consumer_db` to `consumerpostgres\consumer_db` which is on port `1003`

- For the communication between backends, we needed to do the following change:

  - from `http://localhost:8082/api/usertable/..` to `http://backendproduct:8082/api/usertable/..`

- For the communication between backends and RabbitMQ, we needed to do the following change:

  - from `http://localhost:5672/..` to `http://rabbitmq:5672/..`

  As it can be seen, the `device simulation` project is independent of docker. This allows for a more realistic behaviour, where the simulation/device is independent of the Docker and just sends to the queue information on `localhost`.

<div style="page-break-after: always;"></div>

## Build and execution considerations

Open cmd in the root folder and run

        docker-compose up --build

After the run, a set of containers should be deployed. In order to see, I recommend using Docker Desktop. Consumer Backend might be blocked by the absence of RabbitMQ, so a restart of the container might be necessary.

In order to access the page, go to:

        http://localhost:3000/

or:

        http://localhost:3000/login

The data for the databases is stored in `root/init_db/...` which is ran at initialization of the database containers

In order to access the already registered users, use their email as password. For example:

        email = admin@admin.com, password = admin@admin.com
        email = dada@gmail.com, password = dada@gmail.com

## Conclusions

In order to create scalable applications that can employ a varied number of people and different technologies, the usage of Docker and container style deployment is necessary. The current project is rather simple in its scope and use, but in reality there can be a variety of services, each written in a different language and uses different types of server, which would be imposible to implement with a monolitic architecture in mind.
