CREATE SCHEMA IF NOT EXISTS consumer_db;

-- Set the search path to include the 'consumer_db' schema
SET search_path TO consumer_db;

----------------------------------------------------------------------------------- user product relationship creation
-- Drop the table if it exists
DROP TABLE IF EXISTS consumer_db.user_product;

-- Create a new table in the consumer_db schema
CREATE TABLE consumer_db.user_product
(
    ID           SERIAL       PRIMARY KEY,
    USER_ID    integer      NOT NULL,
    PRODUCT_ID integer      NOT NULL,
	MAX_USAGE integer NOT NULL
);

-- Set the owner of the table
ALTER TABLE consumer_db.user_product OWNER TO "compose-postgres";

----------------------------------------------------------------------------------- consumption table creation
-- Drop the table if it exists
DROP TABLE IF EXISTS consumer_db.consumption;

-- Create a new table in the consumer_db schema
CREATE TABLE consumer_db.consumption
(
    ID           SERIAL       PRIMARY KEY,
    RELATIONSHIP_ID integer NOT NULL
		constraint user_product_relationship_fk
				references consumer_db.user_product
				on update cascade on delete cascade,
	USAGE_TIMESTAMP float NOT NULL,
    USAGE  float NOT NULL
);

-- Set the owner of the table
ALTER TABLE consumer_db.consumption OWNER TO "compose-postgres";

-- Reset the search path to its default value
RESET search_path;
