CREATE SCHEMA IF NOT EXISTS user_db;

-- Set the search path to include the 'user_db' schema
SET search_path TO user_db;

----------------------------------------------------------------------------------- user table creation
-- Drop the table if it exists
DROP TABLE IF EXISTS user_db.users_table;

-- Create a new table in the user_db schema
CREATE TABLE user_db.users_table
(
    ID       SERIAL       PRIMARY KEY,
    EMAIL    text NOT NULL,
    PASSWORD text NOT NULL,
	DESCRIPTION text
);

-- Set the owner of the table
ALTER TABLE user_db.users_table OWNER TO "compose-postgres";

----------------------------------------------------------------------------------- Data insertion
insert into user_db.users_table (email, password, description) values ('admin@admin.com', '$2a$10$EXafbnaimlkrtfaJugLj6.A7QhsA8cQZXsxJmuHoP24VZ50XWME4i', '');
insert into user_db.users_table (email, password, description) values ('dada@gmail.com', '$2a$10$qyxcLcDQBswcud2.LEVL8OAdCJo.G7LYxctHbWXjggAd2U7R4g0Fa', '');
insert into user_db.users_table (email, password, description) values ('nunu@gmail.com', '$2a$10$b6f09gcYYy16XFuKMJDm1O0.BOcCMa45nN16b3mhBWZiBJH1SOP0u', '');
insert into user_db.users_table (email, password, description) values ('yeye@gmail.com', '$2a$10$AUaQ0VcVmJJ1Krrr1bKcJuxs5LZ5epRdn5RSpWalLlF/fZt.kBqJ2', '');
insert into user_db.users_table (email, password, description) values ('sasa@gmail.com', '$2a$10$qp3xYF7vT8NRf5Va8aZjlesNFZlw/Ch8kJJmOqMf/NW/pIqTyWI5C', '');

-- Reset the search path to its default value
RESET search_path;