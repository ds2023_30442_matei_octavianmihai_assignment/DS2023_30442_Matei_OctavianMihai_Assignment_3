import * as React from 'react'

import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle'
import Box from '@mui/material/Box'
import TextField from '@mui/material/TextField'
import ChatApi from '../common/ChatApi'
import SeenPng from '../images/seen.png'

export default function ChatDialog (props) {
  const isTyping = () => {
    const milliseconds = 1 * 1000
    const possible = ChatApi.whoIsTyping.filter(
      e =>
        e.from == props.to &&
        e.date <= ChatApi.getFormattedTime(milliseconds) &&
        e.date >= ChatApi.getFormattedTime(-milliseconds)
    )
    if (possible.length > 0) {
      return '...is typing'
    } else {
      return ''
    }
  }
  const decideSeen = message => {
    let available = ChatApi.whoIsActive.filter(e => {
      return e.from == message.to
    })[0]
    if (available != null) {
      if (available.text == 'Enter') return true
      else if (available.date >= message.date) return true
      else return false
    } else return false
  }
  const handleSubmit = event => {
    event.preventDefault()
    const formData = new FormData(event.currentTarget)

    const data = {
      to: props.to,
      text: formData.get('message')
    }
    ChatApi.sendPrivateMessage(data)
    event.target.reset()
  }
  const handleChange = event => {
    event.preventDefault()
    const formData = new FormData(event.currentTarget)

    const data = {
      to: props.to,
      text: formData.get('message')
    }
    ChatApi.sendPrivateTyping(data)
  }
  let dialog = (
    <Dialog
      open={props.open}
      onClose={props.handleClose}
      aria-labelledby='alert-dialog-title'
      aria-describedby='alert-dialog-description'
      PaperProps={{
        sx: {
          minWidth: '30%',
          minheight: 300,
          width: 'auto',
          maxHeight: 'auto'
        }
      }}
    >
      <DialogTitle id='alert-dialog-title'>Message to {props.to}</DialogTitle>
      <DialogContent>
        <DialogContentText style={{ color: 'blue' }}>
          {isTyping()}
        </DialogContentText>
        {ChatApi.all_messages
          .filter(message => {
            return (
              (message.to == props.fromJwt && message.from == props.to) ||
              message.to == props.to
            )
          })
          .map(message => (
            <DialogContentText>
              <DialogContentText> </DialogContentText>
              <DialogContentText>{message.text}</DialogContentText>
              <DialogContentText
                display='flex'
                justifyContent='right'
                alignItems='right'
              >
                {decideSeen(message) == true ? (
                  <img
                    src={SeenPng}
                    alt=''
                    style={{ width: '20px', height: 'auto' }}
                  />
                ) : (
                  ''
                )}
                {message.from} AT {message.date}
              </DialogContentText>
            </DialogContentText>
          ))}
      </DialogContent>
      <DialogActions>
        <Box
          component='form'
          onSubmit={handleSubmit}
          fullWidth
          onChange={handleChange}
        >
          <TextField
            margin='normal'
            required
            fullWidth
            id='message'
            label='message'
            name='message'
            autoFocus
          />
          <Button
            type='submit'
            fullWidth
            variant='contained'
            sx={{ mt: 3, mb: 2 }}
          >
            Send
          </Button>
        </Box>
      </DialogActions>

      <DialogActions>
        <Button
          onClick={props.handleClose}
          autoFocus
          fullWidth
          variant='contained'
          sx={{ mt: 3, mb: 2 }}
        >
          {' '}
          Close{' '}
        </Button>
      </DialogActions>
    </Dialog>
  )

  if (!props.open) dialog = null

  return dialog
}
