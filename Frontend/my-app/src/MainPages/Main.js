import * as React from 'react'

import Container from '@mui/material/Container'
import { createTheme, ThemeProvider } from '@mui/material/styles'
import { useNavigate } from 'react-router-dom'

import LocalStorageHelper from '../common/localStorageMethods'
import Admin from './Admin'
import Client from './Client'

import Avatar from '@mui/material/Avatar'
import Button from '@mui/material/Button'
import Box from '@mui/material/Box'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined'
import Typography from '@mui/material/Typography'
import ChatApi from '../common/ChatApi'

const theme = createTheme()

export default function Main () {
  let navigate = useNavigate()
  const handleLogout = event => {
    ChatApi.onLogout()
    event.preventDefault()

    LocalStorageHelper.logout()
    navigate('/login')
  }
  React.useEffect(() => {
    if (localStorage.getItem('user') == null) return
  })

  return (
    <div>
      <ThemeProvider theme={theme}>
        <Container maxWidth='xs'>
          <Box
            sx={{
              marginTop: 8,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center'
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component='h1' variant='h5'>
              Welcome {LocalStorageHelper.getUser().email}
            </Typography>
            <Button
              type='submit'
              fullWidth
              variant='contained'
              onClick={handleLogout}
              sx={{ mt: 3, mb: 2 }}
            >
              Logout
            </Button>
          </Box>
        </Container>
      </ThemeProvider>
      {LocalStorageHelper.getUser().isAdmin === true ? <Admin /> : <Client />}
    </div>
  )
}
