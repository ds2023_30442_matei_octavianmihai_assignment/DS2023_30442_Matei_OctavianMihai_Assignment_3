import React, { useState, useEffect } from 'react'

import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Button,
  TextField,
  Grid,
  Typography
} from '@mui/material'
import config from '../../config.json'
import LocalStorageHelper from '../../common/localStorageMethods'
import ChatApi from '../../common/ChatApi'
import { makeStyles } from '@mui/styles'
import ChatDialog from '../../pop_messages/ChatDialog'
import { Chat } from '@mui/icons-material'

const API_FETCH_EVERYTHING = config.userApi + 'users/get_all_users'
const API_UPDATE_USER = config.userApi + 'users/update_user'
const API_DELETE_USER = config.userApi + 'users/delete_user'

const useStyles = makeStyles({
  actionButton: {
    backgroundColor: '#007BFF',
    color: 'white',
    marginRight: '5px'
  },
  editButton: {
    backgroundColor: '#28a745',
    color: 'white',

    marginRight: '5px'
  }
})

function UserTable () {
  const classes = useStyles()
  const [data, setData] = useState([])
  const [updatedRow, setUpdatedRow] = useState([])
  const [newEntry, setNewEntry] = useState({
    email: '',
    password: '',
    description: ''
  })
  const [notification, setNotification] = useState('')
  const [editMode, setEditMode] = useState(false)
  const [availableUsers, addAvailableUsers] = useState([])
  const [openChat, setOpenChat] = React.useState(false)
  const [chatTarget, setChatTarget] = React.useState('')

  useEffect(() => {
    if (ChatApi.userData.connected === false) {
      ChatApi.connect(
        LocalStorageHelper.getJwtUser(),
        LocalStorageHelper.getUser().email
      )
    } else {
      ChatApi.sendPrivateValue(ChatApi.possibleAddresses.fetch_users)
      addAvailableUsers(ChatApi.availableUsers)
    }
    // Fetch data from your API when the component mounts
    const requestOptions = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      }
    }

    fetch(
      API_FETCH_EVERYTHING + '/' + LocalStorageHelper.getJwtUser(),
      requestOptions
    )
      .then(response => response.json())
      .then(response => {
        if (response.httpStatusCode !== 200) throw new Error(response.message)
        setData(response.data)
      })
      .catch()
  }, [data, notification, availableUsers])

  const handleSave = rowData => {
    setNotification('')
    // Send a PUT request to update the row data
    const data_input = {
      jwt: LocalStorageHelper.getJwtUser(),
      commonUserDto: rowData
    }
    const requestOptions = {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      body: JSON.stringify(data_input)
    }

    fetch(API_UPDATE_USER, requestOptions)
      .then(response => response.json())
      .then(response => {
        if (response.httpStatusCode !== 200) throw new Error(response.message)
        setNotification('Row modified successfully.')
      })
      .catch(err => {
        setNotification('Row modified failed.')
      })
  }

  const handleDelete = row_data => {
    setNotification('')
    const requestOptions = {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      }
    }

    fetch(
      API_DELETE_USER +
        '/' +
        LocalStorageHelper.getJwtUser() +
        '/' +
        row_data.id,
      requestOptions
    )
      .then(response => response.json())
      .then(response => {
        if (response.httpStatusCode !== 200) throw new Error(response.message)
        setNotification('Row deleted.')
      })
      .catch(err => {
        setNotification('Row could not be deleted.')
      })
  }
  const handleEditClick = rowId => {
    setNotification('')
    setEditMode(!editMode)
    handleSave(updatedRow)
  }

  const userStatus = user => {
    if (availableUsers.includes(user)) {
      return true
    } else {
      return false
    }
  }

  const supportFunction = email => {
    const data = {
      to: email,
      text: openChat ? 'Exit' : 'Enter'
    }
    ChatApi.sendSeenStatus(data)
    setChatTarget(email)
    setOpenChat(!openChat)
  }

  const handleAddNewEntry = () => {
    // Send a POST request to add a new entry
    setNotification('')
    const data_input = {
      jwt: LocalStorageHelper.getJwtUser(),
      commonUserDto: newEntry
    }
    const requestOptions = {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      body: JSON.stringify(data_input)
    }

    fetch(API_UPDATE_USER, requestOptions)
      .then(response => response.json())
      .then(response => {
        if (response.httpStatusCode !== 200) throw new Error(response.message)

        setData([...data, { id: response.data.id, ...newEntry }])
        setNewEntry({ email: '', password: '', description: '' })
        setNotification('Row added.')
      })
      .catch(err => {
        setNotification('Row could not be added.')
      })
  }

  return (
    <div>
      <div>
        <ChatDialog
          id={'user-message' + chatTarget}
          fromJwt={LocalStorageHelper.getJwtUser()}
          from={LocalStorageHelper.getUser().email}
          open={openChat}
          to={chatTarget}
          handleClose={() => supportFunction(chatTarget)}
        ></ChatDialog>
        <Typography variant='h3' gutterBottom>
          List of Users
        </Typography>
        <Grid container spacing={2} alignItems='center'>
          <Grid item>
            <TextField
              label='Email'
              type='text'
              variant='outlined'
              value={newEntry.email}
              onChange={e =>
                setNewEntry({ ...newEntry, email: e.target.value })
              }
              className={classes.input}
            />
          </Grid>
          <Grid item>
            <TextField
              label='Password'
              type='text'
              variant='outlined'
              value={newEntry.password}
              onChange={e =>
                setNewEntry({ ...newEntry, password: e.target.value })
              }
              className={classes.input}
            />
          </Grid>
          <Grid item>
            <TextField
              label='Description'
              type='text'
              variant='outlined'
              value={newEntry.description}
              onChange={e =>
                setNewEntry({ ...newEntry, description: e.target.value })
              }
              className={classes.input}
            />
          </Grid>
          <Grid item>
            <Button
              variant='contained'
              className={classes.button}
              onClick={handleAddNewEntry}
            >
              Add New Entry
            </Button>
          </Grid>
        </Grid>
      </div>
      {notification && <div className='notification'>{notification}</div>}
      <TableContainer sx={{ maxHeight: 440, maxWidth: '90%', margin: 'auto' }}>
        <Table stickyHeader aria-label='sticky table'>
          <TableHead>
            <TableRow>
              <TableCell>Status </TableCell>
              <TableCell>ID </TableCell>
              <TableCell>Email</TableCell>
              <TableCell>New Password</TableCell>
              <TableCell>Description</TableCell>
              <TableCell align='center' colSpan={2}>
                Action
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map(row => (
              <TableRow key={row.id}>
                <TableCell suppressContentEditableWarning='true'>
                  {userStatus(row.email) ? 'Online' : 'Offline'}
                  <Button
                    disabled={!userStatus(row.email)}
                    onClick={() => supportFunction(row.email)}
                  >
                    Chat
                  </Button>
                </TableCell>
                <TableCell>{row.id}</TableCell>
                <TableCell>{row.email}</TableCell>
                <TableCell
                  contentEditable
                  suppressContentEditableWarning='true'
                  onBlur={e => {
                    const updatedRow = { ...row, password: e.target.innerText }
                    setUpdatedRow(updatedRow)
                  }}
                >
                  {}
                </TableCell>
                <TableCell
                  contentEditable
                  suppressContentEditableWarning='true'
                  onBlur={e => {
                    const updatedRow = {
                      ...row,
                      description: e.target.innerText
                    }
                    setUpdatedRow(updatedRow)
                  }}
                >
                  {row.description}
                </TableCell>
                <TableCell>
                  <Button
                    variant='contained'
                    className={classes.editButton}
                    onClick={() => handleEditClick(row.id)}
                  >
                    Edit
                  </Button>
                </TableCell>
                <TableCell>
                  <Button
                    variant='contained'
                    className={classes.actionButton}
                    onClick={() => handleDelete(row)}
                  >
                    Delete
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  )
}

export default UserTable
