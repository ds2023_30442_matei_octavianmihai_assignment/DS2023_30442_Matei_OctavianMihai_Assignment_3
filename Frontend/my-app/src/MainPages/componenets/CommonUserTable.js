import React, { useState, useEffect } from 'react'

import {
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Button,
  Typography
} from '@mui/material'
import config from '../../config.json'
import LocalStorageHelper from '../../common/localStorageMethods'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import SocketConsumer from '../../common/SocketConsumer'
import ChatApi from '../../common/ChatApi'
import Chart from '../../pop_messages/Chart'
import supportImg from '../../images/support.png'
import ChatDialog from '../../pop_messages/ChatDialog'

function CommonUserTable () {
  const [data, setData] = useState([])
  const [selected_relationship, setRelationship] = useState(null)
  const [selected_max_usage, setMaxUsage] = useState(null)
  const [messages, addMessages] = useState([])
  const [availableUsers, addAvailableUsers] = useState([])
  const [open, setOpen] = React.useState(false)
  const [openChat, setOpenChat] = React.useState(false)

  const getIfOver = relationshipId => {
    var messages_get = messages.find(item => item.pairId === relationshipId)
    if (messages_get)
      return messages_get.alertConsumption
        ? 'rgba(255, 0, 0, 0.5)'
        : 'rgba(0, 100, 52, 0.5)'
    else return 'rgba(0, 100, 52, 0.5)'
  }

  const calculateTotalConsumption = relationshipId => {
    var messages_get = messages.find(item => item.pairId === relationshipId)
    if (messages_get && messages_get.deviceEntityArrayList) {
      return messages_get.deviceEntityArrayList[
        messages_get.deviceEntityArrayList.length - 1
      ].usage
    }
    return 0
  }

  const supportFunction = email => {
    const data = {
      to: email,
      text: openChat ? 'Exit' : 'Enter'
    }
    ChatApi.sendSeenStatus(data)
    setOpenChat(!openChat)
  }

  useEffect(() => {
    if (ChatApi.userData.connected === false) {
      ChatApi.connect(
        LocalStorageHelper.getJwtUser(),
        LocalStorageHelper.getUser().email
      )
    } else {
      ChatApi.sendPrivateValue(ChatApi.possibleAddresses.fetch_users)
      addAvailableUsers(ChatApi.availableUsers)
    }
    if (SocketConsumer.userData.connected === false) {
      SocketConsumer.connect(LocalStorageHelper.getJwtUser())
    }
    if (SocketConsumer.userData.connected === true) {
      SocketConsumer.sendPrivateValue()
      addMessages(SocketConsumer.messages)
    }
    // Fetch data from your API when the component mounts
    const requestOptions = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      }
    }
    fetch(
      config.productApi +
        'userproduct/get_by_id/' +
        LocalStorageHelper.getJwtUser() +
        '/' +
        LocalStorageHelper.getUser().id,
      requestOptions
    )
      .then(response => response.json())
      .then(response => {
        if (response.httpStatusCode !== 200) throw new Error(response.message)
        setData(response.data)
      })
      .catch()
  }, [data, messages, availableUsers])

  const [edit_check_date, set_edit_check_date] = useState(new Date())

  return (
    <div>
      <ChatDialog
        id='user-message'
        open={openChat}
        fromJwt={LocalStorageHelper.getJwtUser()}
        from={LocalStorageHelper.getUser().email}
        to='admin@admin.com'
        handleClose={() => supportFunction('admin@admin.com')}
      ></ChatDialog>
      <Chart
        id='user-chart'
        open={open}
        title='Consumption'
        data={messages}
        max_consumption={selected_max_usage}
        target_date={edit_check_date}
        target_relationship={selected_relationship}
        handleClose={() => {
          setOpen(false)
        }}
      ></Chart>
      <div>
        <Typography
          variant='h3'
          gutterBottom
          sx={{
            margin: '30px'
          }}
        >
          List of Products
        </Typography>
      </div>
      <TableContainer
        component={Paper}
        sx={{ maxHeight: 440, maxWidth: '90%', margin: 'auto' }}
      >
        <Table stickyHeader aria-label='sticky table'>
          <TableHead>
            <TableRow>
              <TableCell>Relationship ID</TableCell>
              <TableCell>Product Name</TableCell>
              <TableCell>Max Consumption</TableCell>
              <TableCell>Over Maximum</TableCell>
              <TableCell>See graph</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map(row => (
              <TableRow key={row.userProductDto.id}>
                <TableCell>{row.userProductDto.id}</TableCell>
                <TableCell>{row.productDto.productName}</TableCell>
                <TableCell>{row.productDto.maxUsage}</TableCell>
                <TableCell
                  style={{ backgroundColor: getIfOver(row.userProductDto.id) }}
                >
                  {calculateTotalConsumption(row.userProductDto.id)}
                </TableCell>
                <TableCell>
                  <DatePicker
                    dateFormat='dd/MM/yyyy'
                    className='form-control'
                    selected={edit_check_date}
                    onChange={date => {
                      set_edit_check_date(date)
                    }}
                  />
                  <Button
                    onClick={event => {
                      setRelationship(row.userProductDto.id)
                      setMaxUsage(row.productDto.maxUsage)
                      setOpen(true)
                    }}
                  >
                    See graph
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

      <Button
        variant='contained'
        onClick={() => supportFunction('admin@admin.com')}
        sx={{
          margin: '30px'
        }}
      >
        <img src={supportImg} alt='Su' width={'30px'} />
        &nbsp;&nbsp;{`Support`}
      </Button>
    </div>
  )
}

export default CommonUserTable
