import React, { useState, useEffect } from 'react'

import {
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Button,
  TextField,
  Grid,
  Typography
} from '@mui/material'
import config from '../../config.json'
import { makeStyles } from '@mui/styles'
import LocalStorageHelper from '../../common/localStorageMethods'

const API_FETCH_EVERYTHING = config.productApi + 'product/get_all_products'
const API_UPDATE = config.productApi + 'product/update_product'
const API_DELETE = config.productApi + 'product/delete_product'

const useStyles = makeStyles({
  actionButton: {
    backgroundColor: '#007BFF',
    color: 'white',
    marginRight: '5px'
  },
  editButton: {
    backgroundColor: '#28a745',
    color: 'white',

    marginRight: '5px'
  }
})
function ProductTable () {
  const classes = useStyles()
  const [data, setData] = useState([])
  const [updatedRow, setUpdatedRow] = useState([])
  const [newEntry, setNewEntry] = useState({
    id: null,
    productName: '',
    description: '',
    maxUsage: '',
  })
  const [notification, setNotification] = useState('')
  const [editMode, setEditMode] = useState(false)

  useEffect(() => {
    // Fetch data from your API when the component mounts
    const requestOptions = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      }
    }

    fetch(API_FETCH_EVERYTHING + '/' + LocalStorageHelper.getJwtUser(), requestOptions)
      .then(response => response.json())
      .then(response => {
        if (response.httpStatusCode !== 200) throw new Error(response.message)
        setData(response.data)
      })
      .catch()
  }, [notification])

  const handleSave = rowData => {
    // Send a PUT request to update the row data
    const requestOptions = {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      body: JSON.stringify({
        productDto: rowData, 
        jwt: LocalStorageHelper.getJwtUser()
      })
    }

    console.log(requestOptions)

    fetch(API_UPDATE, requestOptions)
      .then(response => response.json())
      .then(response => {
        if (response.httpStatusCode !== 200) throw new Error(response.message)
        setNotification('Row modified successfully.')
      })
      .catch(err => {
        setNotification('Row modified failed.')
      })
  }

  const handleDelete = row_data => {
    // Send a DELETE request to delete the row
    const requestOptions = {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
    }

    fetch(API_DELETE + '/' + LocalStorageHelper.getJwtUser() + '/' + row_data.id, requestOptions)
      .then(response => response.json())
      .then(response => {
        if (response.httpStatusCode !== 200) throw new Error(response.message)
        setNotification('Row deleted.')
      })
      .catch(err => {
        setNotification('Row could not be deleted.')
      })
  }
  const handleEditClick = rowId => {
    // Toggle edit mode
    setEditMode(!editMode)
    handleSave(updatedRow)
  }
  const handleAddNewEntry = () => {
    // Send a PATCH request to add a new entry
    const requestOptions = {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      body:  JSON.stringify({
        productDto: newEntry, 
        jwt: LocalStorageHelper.getJwtUser()
      })
    }

    fetch(API_UPDATE, requestOptions)
      .then(response => response.json())
      .then(response => {
        if (response.httpStatusCode !== 200) throw new Error(response.message)

        setData([...data, { id: response.data.id, ...newEntry }])
        setNewEntry({ productName: '', description: '' })
        setNotification('Row added.')
      })
      .catch(err => {
        setNotification('Row could not add.')
      })
  }

  return (
    <div>
      <div>
        <Typography variant='h3' gutterBottom>
          List of Products
        </Typography>
        <Grid container spacing={2} alignItems='center'>
          <Grid item>
            <TextField
              label='Product name'
              type='text'
              variant='outlined'
              value={newEntry.productName}
              onChange={e =>
                setNewEntry({ ...newEntry, productName: e.target.value })
              }
              className={classes.input}
            />
          </Grid>
          <Grid item>
            <TextField
              label='Description'
              type='text'
              variant='outlined'
              value={newEntry.description}
              onChange={e =>
                setNewEntry({ ...newEntry, description: e.target.value })
              }
              className={classes.input}
            />
          </Grid>
          <Grid item>
            <TextField
              label='Max Usage'
              type='number'
              variant='outlined'
              value={newEntry.maxUsage}
              onChange={e =>
                setNewEntry({ ...newEntry, maxUsage: e.target.value })
              }
              className={classes.input}
            />
          </Grid>
          <Grid item>
            <Button
              variant='contained'
              className={classes.button}
              onClick={handleAddNewEntry}
            >
              Add New Entry
            </Button>
          </Grid>
        </Grid>
      </div>
      {notification && <div className='notification'>{notification}</div>}
      <TableContainer
        component={Paper}
        sx={{ maxHeight: 440, maxWidth: '90%', margin: 'auto' }}
      >
        <Table stickyHeader aria-label='sticky table'>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Description</TableCell>
              <TableCell>MaxUsage</TableCell>
              <TableCell align='center' colSpan={2}>
                Action
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map(row => (
              <TableRow key={row.id}>
                <TableCell>{row.id}</TableCell>
                <TableCell
                  contentEditable
                  suppressContentEditableWarning='true'
                  onBlur={e => {
                    const updatedRow = {
                      ...row,
                      productName: e.target.innerText
                    }
                    setUpdatedRow(updatedRow)
                  }}
                >
                  {row.productName}
                </TableCell>
                <TableCell
                  contentEditable
                  suppressContentEditableWarning='true'
                  onBlur={e => {
                    const updatedRow = {
                      ...row,
                      description: e.target.innerText
                    }
                    setUpdatedRow(updatedRow)
                  }}
                >
                  {row.description}
                </TableCell>
                <TableCell
                  contentEditable
                  suppressContentEditableWarning='true'
                  onBlur={e => {
                    const updatedRow = {
                      ...row,
                      maxUsage: e.target.innerText
                    }
                    setUpdatedRow(updatedRow)
                  }}
                >
                  {row.maxUsage}
                </TableCell>
                <TableCell>
                  <Button
                    variant='contained'
                    className={classes.editButton}
                    onClick={() => handleEditClick(row.id)}
                  >
                    Edit
                  </Button>
                </TableCell>
                <TableCell>
                  <Button
                    variant='contained'
                    className={classes.actionButton}
                    onClick={() => handleDelete(row)}
                  >
                    Delete
                  </Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  )
}

export default ProductTable
