export default class LocalStorageHelper {
  static logout = () => {
    localStorage.removeItem('user')
  }

  static getUser = () => {
    return JSON.parse(localStorage.getItem('user'))
  }

  static getJwtUser = () => {
    return JSON.parse(localStorage.getItem('user'))['jwt']
  }

  static setUser = user => {
    localStorage.removeItem('user')
    localStorage.setItem('user', JSON.stringify(user))
  }
}
