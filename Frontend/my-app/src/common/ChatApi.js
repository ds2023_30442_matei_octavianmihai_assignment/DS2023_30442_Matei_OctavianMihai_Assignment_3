import config from '../config.json'
import { over } from 'stompjs'
import SockJS from 'sockjs-client'

export default class ChatApi {
  static all_messages = []
  static stompClient = null
  static availableUsers = []
  static userData = {
    fromJwt: '',
    from: '',
    connected: false,
    message: ''
  }
  static whoIsTyping = []
  static whoIsActive = []
  static messages = []
  static possibleAddresses = {
    login: '/app/authenticate',
    logout: '/app/logout',
    fetch_users: '/app/fetch_users',
    send_private: '/app/private-message',
    seen_messages: '/app/seen_messages',
    is_typing: '/app/is_typing'
  }
  constructor () {
    this.initializeUserData()
  }

  initializeUserData () {
    ChatApi.stompClient = null
    ChatApi.availableUsers = []
    ChatApi.all_messages = []
    ChatApi.whoIsActive = []
    ChatApi.userData = {
      fromJwt: '',
      from: '',
      connected: false,
      message: ''
    }
    ChatApi.messages = []
    ChatApi.possibleAddresses = {
      login: '/app/authenticate',
      logout: '/app/logout',
      fetch_users: '/app/fetch_users',
      send_private: '/app/private-message',
      seen_messages: '/app/seen_messages',
      is_typing: '/app/is_typing'
    }
    ChatApi.whoIsTyping = []
  }

  static connect = (jwt, email) => {
    if (!ChatApi.stompClient) {
      ChatApi.userData.fromJwt = jwt
      ChatApi.userData.from = email
      console.log('connect', ChatApi.userData)
      let Sock = new SockJS(config.chatApi)
      ChatApi.stompClient = over(Sock)
      ChatApi.stompClient.connect({}, ChatApi.onConnected, ChatApi.onError)
    }
  }

  static onConnected = () => {
    ChatApi.userData.connected = true
    ChatApi.stompClient.subscribe(
      '/user/' + ChatApi.userData.fromJwt + '/authenticate_response',
      ChatApi.onAuthResponse
    )
    ChatApi.stompClient.subscribe(
      '/user/' + ChatApi.userData.fromJwt + '/fetch_users_response',
      ChatApi.onFetchUsers
    )
    ChatApi.stompClient.subscribe(
      '/user/' + ChatApi.userData.fromJwt + '/typing_response',
      ChatApi.onIsTyping
    )
    ChatApi.stompClient.subscribe(
      '/user/' + ChatApi.userData.fromJwt + '/private',
      ChatApi.onPrivateMessageReceived
    )
    ChatApi.stompClient.subscribe(
      '/user/' + ChatApi.userData.fromJwt + '/seen_messages',
      ChatApi.seenMessages
    )
    ChatApi.sendPrivateValue(ChatApi.possibleAddresses.login)
  }

  static sendPrivateMessage = userData => {
    userData.from = ChatApi.userData.from
    userData.fromJwt = ChatApi.userData.fromJwt
    userData.date = ChatApi.getFormattedTime(0)
    ChatApi.all_messages = [...ChatApi.all_messages, userData]
    if (ChatApi.stompClient) {
      ChatApi.stompClient.send(
        ChatApi.possibleAddresses.send_private,
        {},
        JSON.stringify(userData)
      )
    }
  }

  static sendPrivateTyping = userData => {
    userData.from = ChatApi.userData.from
    userData.fromJwt = ChatApi.userData.fromJwt
    console.log('sendPrivateTyping', userData)
    if (ChatApi.stompClient) {
      console.log('sendPrivateTyping', userData)
      ChatApi.stompClient.send(
        ChatApi.possibleAddresses.is_typing,
        {},
        JSON.stringify(userData)
      )
    }
  }

  static sendSeenStatus = userData => {
    userData.from = ChatApi.userData.from
    userData.fromJwt = ChatApi.userData.fromJwt
    console.log('sendSeenStatus', userData)
    if (ChatApi.stompClient) {
      console.log('sendSeenStatus', userData)
      ChatApi.stompClient.send(
        ChatApi.possibleAddresses.seen_messages,
        {},
        JSON.stringify(userData)
      )
    }
  }

  static sendPrivateValue = (address, message) => {
    if (ChatApi.stompClient) {
      ChatApi.userData.message = message != null ? message : ''
      console.log('SendMessage', ChatApi.userData)
      ChatApi.stompClient.send(address, {}, JSON.stringify(ChatApi.userData))
    }
  }

  static onError = err => {
    if (ChatApi.stompClient) {
      ChatApi.sendPrivateValue('/app/logout')
      ChatApi.stompClient.disconnect()
      ChatApi.userData.connected = false
    }
  }

  static onLogout = () => {
    console.log('Logging out')
    ChatApi.sendPrivateValue('/app/logout')
  }

  static onAuthResponse = payload => {
    var payloadData = JSON.parse(payload.body)
    if (payloadData.text != null) {
      ChatApi.messages = payloadData.text
    }
  }
  static onFetchUsers = payload => {
    var payloadData = JSON.parse(payload.body)
    if (payloadData.length > 0) {
      ChatApi.availableUsers = payloadData
    }
  }
  static onIsTyping = payload => {
    var payloadData = JSON.parse(payload.body)
    ChatApi.whoIsTyping = [
      ...ChatApi.whoIsTyping.filter(
        e => e.date >= ChatApi.getFormattedTime(-5000)
      ),
      { from: payloadData.from, date: ChatApi.getFormattedTime(0) }
    ]
  }
  static onPrivateMessageReceived = payload => {
    var payloadData = JSON.parse(payload.body)
    if (payloadData.text.length > 0) {
      ChatApi.all_messages = [...ChatApi.all_messages, payloadData]
    }
  }

  static seenMessages = payload => {
    var payloadData = JSON.parse(payload.body)
    console.log('seenMessages', payloadData)
    if (ChatApi.whoIsActive.length == 0) {
      ChatApi.whoIsActive = [
        ...ChatApi.whoIsActive,
        {
          from: payloadData.from,
          text: payloadData.text,
          date: ChatApi.getFormattedTime(0)
        }
      ]
    } else {
      ChatApi.whoIsActive = ChatApi.whoIsActive.map(e => {
        if (e.from === payloadData.from) {
          return {
            from: payloadData.from,
            text: payloadData.text,
            date: ChatApi.getFormattedTime(0)
          }
        } else {
          return e
        }
      })

      // Check if the condition was not met for any existing entry
      const entryExists = ChatApi.whoIsActive.some(
        e => e.from === payloadData.from
      )

      // If no entry was found, add a new one
      if (!entryExists) {
        ChatApi.whoIsActive.push({
          from: payloadData.from,
          text: payloadData.text,
          date: ChatApi.getFormattedTime(0)
        })
      }
    }
  }

  static getFormattedTime = delay => {
    let timeObject = new Date()
    timeObject = new Date(timeObject.getTime() + delay)
    const timeComponents = [
      timeObject.getHours(),
      timeObject.getMinutes(),
      timeObject.getSeconds()
    ]
    return timeComponents
      .map(component => {
        const pad = component < 10 ? '0' : ''
        return pad + component
      })
      .join(':')
  }
}
