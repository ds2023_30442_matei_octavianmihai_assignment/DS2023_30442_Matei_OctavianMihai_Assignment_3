import config from '../config.json'
import { over } from 'stompjs'
import SockJS from 'sockjs-client'
import LocalStorageHelper from './localStorageMethods'

export default class SocketConsumer {
  static stompClient = null
  static messages = []
  static userData = {}

  constructor () {
    this.initializeUserData()
  }

  initializeUserData () {
    SocketConsumer.stompClient = null
    SocketConsumer.messages = []
    SocketConsumer.userData = {
      from: LocalStorageHelper.getJwtUser(),
      connected: false
    }
  }

  static connect = jwt => {
    if (!SocketConsumer.stompClient) {
      SocketConsumer.userData.from = jwt
      let Sock = new SockJS(config.consumerApi)
      SocketConsumer.stompClient = over(Sock)
      SocketConsumer.stompClient.connect(
        {},
        SocketConsumer.onConnected,
        SocketConsumer.onError
      )
    }
  }

  static onConnected = () => {
    SocketConsumer.userData.connected = true
    SocketConsumer.stompClient.subscribe(
      '/user/' + SocketConsumer.userData.from + '/private',
      SocketConsumer.onPrivateMessage
    )
    SocketConsumer.sendPrivateValue()
  }

  static sendPrivateValue = () => {
    if (SocketConsumer.stompClient) {
      SocketConsumer.stompClient.send(
        '/app/private-message',
        {},
        JSON.stringify(SocketConsumer.userData)
      )
    }
  }

  static onError = err => {
    if (SocketConsumer.stompClient) {
      SocketConsumer.stompClient.disconnect()
      SocketConsumer.userData.connected = false
    }
  }

  static onPrivateMessage = payload => {
    if (SocketConsumer.stompClient) {
      var payloadData = JSON.parse(payload.body)
      let obj = JSON.parse(payloadData.text)
      SocketConsumer.messages = obj
    }
  }
}
