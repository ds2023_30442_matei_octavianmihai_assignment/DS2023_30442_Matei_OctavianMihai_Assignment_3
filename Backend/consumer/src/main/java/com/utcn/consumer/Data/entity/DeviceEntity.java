package com.utcn.consumer.Data.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.utcn.consumer.Common.Mapper.MyDoubleDesirializer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Entity
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "consumption", schema="consumer_db")
public class DeviceEntity {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "relationship_id", nullable = false)
    private int pairId;
    @Column(name = "usage_timestamp", nullable = false)
    @JsonSerialize(using= MyDoubleDesirializer.class)
    private Double timestamp;
    @Column(name = "usage", nullable = false)
    @JsonSerialize(using= MyDoubleDesirializer.class)
    private Double usage;

    @Override
    public String toString() {
        return "DeviceEntity{" +
                "id=" + id +
                ", pairId=" + pairId +
                ", timestamp=" + timestamp +
                ", usage=" + usage +
                '}';
    }


}

