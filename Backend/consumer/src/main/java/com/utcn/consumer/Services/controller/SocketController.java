package com.utcn.consumer.Services.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.utcn.consumer.Services.Security.JwtTokenUtil;
import com.utcn.consumer.Services.Services.Socket.SocketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class SocketController {


    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;
    @Autowired
    private SocketService socketService;

    @MessageMapping("/private-message")
    public OutputMessage send(@Payload Message msg) {
        OutputMessage message = new OutputMessage();

        if(jwtTokenUtil.isLogged(msg.getFrom())){

            try{

                String message_consumption = socketService.getAllConsumptionByUserId(jwtTokenUtil.getUserId(msg.getFrom()));
                message.setText(message_consumption);
            }catch (JsonProcessingException ex){
                message.setText("Could not fetch data!");
            }
        }else{
            message.setText("Please be authorised!");
        }
        String fromText = "server-side";
        message.setFrom(fromText);
        message.setDate(new SimpleDateFormat("HH:mm:ss").format(new Date()));
        simpMessagingTemplate.convertAndSendToUser(msg.getFrom(), "/private", message);
        return message;
    }
}
