package com.utcn.consumer.Services.Services.Socket;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.utcn.consumer.Data.dto.ConsumptionDto;
import com.utcn.consumer.Data.entity.DeviceEntity;
import com.utcn.consumer.Data.entity.UserProductEntity;
import com.utcn.consumer.Data.repository.DeviceEntityRepository;
import com.utcn.consumer.Data.repository.UserProductEntityRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
public class SocketService {

    private final DeviceEntityRepository deviceEntityRepository;
    private final UserProductEntityRepository userProductEntityRepository;


    public SocketService(DeviceEntityRepository deviceEntityRepository, UserProductEntityRepository userProductEntityRepository) {
        this.deviceEntityRepository = deviceEntityRepository;
        this.userProductEntityRepository = userProductEntityRepository;
    }

    public String getAllConsumptionByUserId(int userId) throws JsonProcessingException {
        ArrayList<ConsumptionDto> userConsumption =new ArrayList<>();
        List<UserProductEntity> userProductEntityList = userProductEntityRepository.findAllByUserId(userId);
        userProductEntityList.sort(Comparator.comparingLong(UserProductEntity::getId));
        for (UserProductEntity entity : userProductEntityList) {
            ConsumptionDto consumptionDto = new ConsumptionDto();
            consumptionDto.setMaxUsage(entity.getMaxUsage());
            consumptionDto.setPairId(Math.toIntExact(entity.getId()));
            List<DeviceEntity> deviceEntities = deviceEntityRepository.findAllByPairId(Math.toIntExact(entity.getId()));
            deviceEntities.sort(Comparator.comparingDouble(DeviceEntity::getTimestamp));
            for (DeviceEntity deviceEntity : deviceEntities) {
                consumptionDto.addDevice(deviceEntity);
            }
            if(deviceEntities.size() > 0){
                consumptionDto.setAlertConsumption(deviceEntities.get(deviceEntities.size()-1).getUsage() >= entity.getMaxUsage());
            }
            userConsumption.add(consumptionDto);
        }

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        return ow.writeValueAsString(userConsumption);
    }
}
