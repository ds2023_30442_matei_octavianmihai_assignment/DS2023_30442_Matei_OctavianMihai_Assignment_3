package com.utcn.consumer.Services.Services.RabbitMQLogic;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.utcn.consumer.Data.rabbitmq.ProductData;
import com.utcn.consumer.Data.rabbitmq.ProductDataProduct;
import com.utcn.consumer.Data.rabbitmq.RabbitMqEntity;
import lombok.SneakyThrows;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;


@Component
public class RabbityMQConsumerProducts extends RabbitMQConsumer{

    private final ProductDataService productDataService;
    private final String property_queue = "rabbitmq.queue-name-product";

    public RabbityMQConsumerProducts(Environment environment, RabbitMqEntity rabbitMqEntity, ProductDataService productDataService) {
        super(rabbitMqEntity);
        this.productDataService = productDataService;
        this.initConsumer(environment.getProperty(property_queue));
    }

    @SneakyThrows
    @RabbitListener(queues = "${"+property_queue+"}")
    public void recievedMessage(String json) {
        try{
            ObjectMapper mapper = new ObjectMapper();
            ProductData productData = mapper.readValue(json, ProductData.class);
            System.out.println("Received Message From RabbitMQ: " + productData);

            productDataService.decideUsageProductData(productData);
        }catch (Exception ex){
            ObjectMapper mapper = new ObjectMapper();
            ProductDataProduct productData = mapper.readValue(json, ProductDataProduct.class);
            System.out.println("Received Message From RabbitMQ: " + productData);

            productDataService.decideProduct(productData);
        }
    }

}
