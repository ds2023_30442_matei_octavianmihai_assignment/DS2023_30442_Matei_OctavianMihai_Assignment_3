package com.utcn.consumer.Common.Mapper;

import com.utcn.consumer.Data.entity.DeviceEntity;
import com.utcn.consumer.Data.entity.UserProductEntity;
import com.utcn.consumer.Data.rabbitmq.DeviceData;
import com.utcn.consumer.Data.rabbitmq.ProductDto;
import com.utcn.consumer.Data.rabbitmq.UserProductDto;
import org.mapstruct.Mapper;

import java.util.ArrayList;

@Mapper(
        componentModel = "spring"
)
public interface MapStructMapper {

    UserProductEntity userProductDtoToUserProductEntity(UserProductDto userProductDto);
    DeviceEntity deviceDataToDeviceEntity(DeviceData deviceData);
    //DeviceEntity deviceData(DeviceEntity device);
}
