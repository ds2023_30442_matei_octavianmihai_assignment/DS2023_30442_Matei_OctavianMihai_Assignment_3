package com.utcn.consumer.Services.Services.RabbitMQLogic;

import com.utcn.consumer.Common.Mapper.MapStructMapperImpl;
import com.utcn.consumer.Data.entity.UserProductEntity;
import com.utcn.consumer.Data.rabbitmq.ProductData;
import com.utcn.consumer.Data.rabbitmq.ProductDataProduct;
import com.utcn.consumer.Data.rabbitmq.ProductDto;
import com.utcn.consumer.Data.rabbitmq.RabbitStatus;
import com.utcn.consumer.Data.repository.UserProductEntityRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class ProductDataService {

    private final UserProductEntityRepository userProductEntityRepository;

    public ProductDataService(UserProductEntityRepository userProductEntityRepository) {
        this.userProductEntityRepository = userProductEntityRepository;
    }

    public void decideUsageProductData(ProductData productData){
        if(Objects.equals(productData.getRabbitStatus(), RabbitStatus.NEW.toString())){
            insertProductData(productData);
        }else if(Objects.equals(productData.getRabbitStatus(), RabbitStatus.UPDATE.toString())){
            updateProductData(productData);
        }else if(Objects.equals(productData.getRabbitStatus(), RabbitStatus.DELETE.toString())){
            deleteProductData(productData);
        }else if(Objects.equals(productData.getRabbitStatus(), RabbitStatus.PURGE.toString())){
            purgeProductData();
        }
    }

    public void insertProductData(ProductData productData){
        MapStructMapperImpl mapStructMapper = new MapStructMapperImpl();
        UserProductEntity userProductEntity = mapStructMapper.userProductDtoToUserProductEntity(productData.getObjectPayload());
        userProductEntity.setMaxUsage(productData.getMax_usage());
        userProductEntityRepository.saveAndFlush(userProductEntity);
    }

    public void updateProductData(ProductData productData){
        MapStructMapperImpl mapStructMapper = new MapStructMapperImpl();
        UserProductEntity userProductEntity = mapStructMapper.userProductDtoToUserProductEntity(productData.getObjectPayload());
        userProductEntity.setMaxUsage(productData.getMax_usage());
        userProductEntityRepository.saveAndFlush(userProductEntity);
    }

    public void deleteProductData(ProductData productData){
        userProductEntityRepository.deleteById(productData.getObjectPayload().getId());
    }

    public void purgeProductData(){
        userProductEntityRepository.deleteAll();
    }

    public void decideProduct(ProductDataProduct productDataProduct){
        if(Objects.equals(productDataProduct.getRabbitStatus(), RabbitStatus.UPDATE.toString())){
            modifyMaxUsage(productDataProduct.getMax_usage(), productDataProduct.getObjectPayload());
        }else if(Objects.equals(productDataProduct.getRabbitStatus(), RabbitStatus.DELETE.toString())) {
            deleteFromProduct(productDataProduct.getObjectPayload().getId());
        }
    }

    public void modifyMaxUsage(int newMaxUsage, ProductDto productDto){
        List<UserProductEntity> byId = userProductEntityRepository.findAllByProductId(Math.toIntExact(productDto.getId()));
        byId.forEach(userProductEntity ->{
            userProductEntity.setMaxUsage(newMaxUsage);
            userProductEntityRepository.saveAndFlush(userProductEntity);
        });
    }

    public void deleteFromProduct(long id){
        userProductEntityRepository.deleteAllByProductId(Math.toIntExact(id));
    }
}
