package com.utcn.consumer.Services.Services.RabbitMQLogic;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.utcn.consumer.Data.rabbitmq.RabbitMqEntity;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RabbitMQConsumer {

    private final RabbitMqEntity rabbitMqEntity;

    public RabbitMQConsumer(RabbitMqEntity rabbitMqEntity) {
        this.rabbitMqEntity = rabbitMqEntity;
    }

    public void initConsumer(String queue_name){
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(rabbitMqEntity.getHost());
        factory.setPort(rabbitMqEntity.getPort());
        factory.setUsername(rabbitMqEntity.getUsername());
        factory.setPassword(rabbitMqEntity.getPassword());
        try{
            factory.getClientProperties();
            int maxAttempts = 50;
            Connection connection = this.makeConnection(factory, maxAttempts);
            Channel channel = connection.createChannel();
            channel.queueDeclare(queue_name, false, false, false, null);
            System.out.println("Queue " + queue_name + " created");
            channel.close();
            connection.close();
        }catch(IOException | TimeoutException | InterruptedException ex) {
            ex.printStackTrace();
        }
    }
    private Connection makeConnection(ConnectionFactory factory, int maxAttempts) throws TimeoutException, InterruptedException {
        int attempts = 0;
        Connection connection = null;

        while (connection == null && attempts < maxAttempts) {
            try {
                connection = factory.newConnection();
                // If no exception is thrown, the connection is successful
            } catch (Exception e) {
                attempts++;
                System.out.println("Connection attempt #" + attempts + " failed. Retrying in 5 seconds...");
                Thread.sleep(5000);
                // You can add a delay here using Thread.sleep if needed
            }
        }
        if (connection != null) {
            System.out.println("Connection successful!");
            return connection;
            // Continue with your logic using the 'connection' object
        } else {
            throw new TimeoutException();
        }
    }
}
