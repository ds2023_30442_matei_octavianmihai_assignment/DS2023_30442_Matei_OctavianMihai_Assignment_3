package com.utcn.chat.Services.Security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

@Component
public class JwtTokenUtil {

    private final JwtParser jwtParser;

    public JwtTokenUtil(Environment environment){
        String secret_key = environment.getProperty("jwt.secret");
        this.jwtParser = Jwts.parser().setSigningKey(secret_key);
    }

    public boolean isAdmin(String jwt){
        Claims data = this.parseJwtClaims(jwt);
        if(validateClaims(data)) return data.getSubject().equals("admin@admin.com");
        else return false;
    }
    public boolean isLogged(String jwt){
        Claims data = this.parseJwtClaims(jwt);
        return validateClaims(data);
    }

    public ArrayList<String> decryptArray(ArrayList<String> jwts){
        ArrayList<String> decrypted = new ArrayList<>();
        for (String jwt:jwts) {
            decrypted.add(this.getEmail(jwt));
        }
        return decrypted;
    }

    public String selectFromList(ArrayList<String> jwts, String target){
        return jwts.stream().filter(j -> Objects.equals(this.getEmail(j), target)).toList().get(0);
    }

    public Claims parseJwtClaims(String token) {
        return jwtParser.parseClaimsJws(token).getBody();
    }


    public boolean validateClaims(Claims claims) {
        return claims.getExpiration().after(new Date());
    }

    public int getUserId(String jwt){
        Claims data = this.parseJwtClaims(jwt);
        return Integer.parseInt(data.get("id").toString());
    }

    public String getEmail(String jwt){
        Claims data = this.parseJwtClaims(jwt);
        if(validateClaims(data)) return data.getSubject();
        else return "";
    }
}