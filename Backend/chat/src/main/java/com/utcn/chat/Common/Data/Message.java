package com.utcn.chat.Common.Data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Message {
    private String fromJwt;
    private String to;
    private String text;

    @Override
    public String toString() {
        return "Message{" +
                "from='" + fromJwt + '\'' +
                ", to='" + to + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
