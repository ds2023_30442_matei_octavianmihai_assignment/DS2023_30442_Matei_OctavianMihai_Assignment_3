package com.utcn.chat.Services.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.utcn.chat.Common.Data.Message;
import com.utcn.chat.Common.Data.OutputMessage;
import com.utcn.chat.Services.Security.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;

@Controller
public class SocketController {

    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    private ArrayList<String> loggedUsers = new ArrayList<>();

    @MessageMapping("/fetch_users")
    public void sendListOfLoggedUsers(@Payload Message msg) throws JsonProcessingException {
        // Send to the admin the list of logged emails/ids
        String jwt = msg.getFromJwt();
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        if(jwtTokenUtil.isLogged(jwt)){
            String response = "";
            try{
                if(jwtTokenUtil.isAdmin(jwt)){

                    response = ow.writeValueAsString(new LinkedHashSet<>(jwtTokenUtil.decryptArray(loggedUsers)) );
                }
                else{
                    ArrayList<String> temp = new ArrayList<>();
                    temp.add("admin@admin.com");
                    response = ow.writeValueAsString(temp);
                }
            }catch (JsonProcessingException ignored){}
            simpMessagingTemplate.convertAndSendToUser(msg.getFromJwt(), "/fetch_users_response",response);
        }else{
            simpMessagingTemplate.convertAndSendToUser(msg.getFromJwt(), "/fetch_users_response", ow.writeValueAsString("Admin not logged in!"));
        }
    }

    @MessageMapping("/authenticate")
    public OutputMessage connect(@Payload Message msg) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        OutputMessage message = new OutputMessage();

        if(jwtTokenUtil.isLogged(msg.getFromJwt())){
            if(!loggedUsers.contains(msg.getFromJwt())){
                loggedUsers.add(msg.getFromJwt());
            }
            message.setText(ow.writeValueAsString("Connected!"));
        }else{
            message.setText(ow.writeValueAsString("Please be authorised!"));
        }
        message.setDate(new SimpleDateFormat("HH:mm:ss").format(new Date()));
        simpMessagingTemplate.convertAndSendToUser(msg.getFromJwt(), "/authenticate_response", message);
        return message;
    }

    @MessageMapping("/logout")
    public OutputMessage logout(@Payload Message msg) throws JsonProcessingException {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        OutputMessage message = new OutputMessage();

        if(jwtTokenUtil.isLogged(msg.getFromJwt())){
            if(loggedUsers.contains(msg.getFromJwt())){
                loggedUsers.remove(msg.getFromJwt());
            }
            message.setText(ow.writeValueAsString("Logout!"));
        }else{
            message.setText(ow.writeValueAsString("Please be authorised!"));
        }
        message.setDate(new SimpleDateFormat("HH:mm:ss").format(new Date()));
        simpMessagingTemplate.convertAndSendToUser(msg.getFromJwt(), "/authenticate_response", message);
        return message;
    }

    @MessageMapping("/is_typing")
    public OutputMessage isTyping(@Payload Message msg) {
        // TODO Send to the other correspondent that the from user is typing
        OutputMessage message = new OutputMessage();

        if(jwtTokenUtil.isLogged(msg.getFromJwt())){
            message.setText("isTyping");
            message.setFrom(jwtTokenUtil.getEmail(msg.getFromJwt()));
            message.setTo(jwtTokenUtil.selectFromList(this.loggedUsers, msg.getTo()));
            message.setDate(new SimpleDateFormat("HH:mm:ss").format(new Date()));
            simpMessagingTemplate.convertAndSendToUser(message.getTo(), "/typing_response", message);
        }else{
            message.setText("Please be authorised!");
        }
        return message;
    }

    @MessageMapping("/private-message")
    public OutputMessage sendMessage(@Payload Message msg) {
        // TODO Send messages
        OutputMessage message = new OutputMessage();

        if(jwtTokenUtil.isLogged(msg.getFromJwt())){
            message.setText(msg.getText());
            message.setFrom(jwtTokenUtil.getEmail(msg.getFromJwt()));
            message.setTo(jwtTokenUtil.selectFromList(this.loggedUsers, msg.getTo()));
            message.setDate(new SimpleDateFormat("HH:mm:ss").format(new Date()));
            simpMessagingTemplate.convertAndSendToUser(message.getTo(), "/private", message);
        }else{
            message.setText("Please be authorised!");
        }
        return message;
    }

    @MessageMapping("/seen_messages")
    public OutputMessage seenMessages(@Payload Message msg) {
        // TODO Send messages
        OutputMessage message = new OutputMessage();

        if(jwtTokenUtil.isLogged(msg.getFromJwt())){
            message.setText(msg.getText());
            message.setFrom(jwtTokenUtil.getEmail(msg.getFromJwt()));
            message.setTo(jwtTokenUtil.selectFromList(this.loggedUsers, msg.getTo()));
            message.setDate(new SimpleDateFormat("HH:mm:ss").format(new Date()));
            simpMessagingTemplate.convertAndSendToUser(message.getTo(), "/seen_messages", message);
        }else{
            message.setText("Please be authorised!");
        }
        return message;
    }

    // TODO Send to the other correspondent that the from user has seen the messages
}
