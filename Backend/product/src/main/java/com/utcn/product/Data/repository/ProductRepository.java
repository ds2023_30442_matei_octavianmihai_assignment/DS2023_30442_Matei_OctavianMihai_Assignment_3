package com.utcn.product.Data.repository;

import com.utcn.product.Data.entity.Product;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;

import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product,Long> {

    @Query("select product from Product product order by product.id")
    @NotNull
    ArrayList<Product> findAll();
}
