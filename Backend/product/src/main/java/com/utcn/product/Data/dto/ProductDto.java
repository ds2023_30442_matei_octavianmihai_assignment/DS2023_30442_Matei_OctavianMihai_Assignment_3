package com.utcn.product.Data.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Getter
@Setter
@Service
public class ProductDto implements Serializable {
    private Long id;
    private String productName;
    private String description;
    private Integer maxUsage;

    @Override
    public String toString() {
        return "ProductDto{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", description='" + description + '\'' +
                ", maxUsage='" + maxUsage + '\'' +
                '}';
    }
}
