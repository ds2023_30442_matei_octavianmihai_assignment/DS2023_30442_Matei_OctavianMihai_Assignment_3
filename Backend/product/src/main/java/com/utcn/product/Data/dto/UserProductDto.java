package com.utcn.product.Data.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Getter
@Setter
@Service
public class UserProductDto implements Serializable {
    @Override
    public String toString() {
        return "UserProductDto{" +
                "id=" + id +
                ", userId=" + userId +
                ", productId=" + productId +
                '}';
    }

    private Long id;
    private Long userId;
    private Long productId;

}
