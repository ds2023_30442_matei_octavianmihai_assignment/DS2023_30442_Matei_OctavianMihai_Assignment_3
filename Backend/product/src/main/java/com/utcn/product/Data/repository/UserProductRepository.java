package com.utcn.product.Data.repository;

import com.utcn.product.Data.entity.UserProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.ArrayList;
import java.util.Optional;
import org.springframework.stereotype.Repository;

@Repository
public interface UserProductRepository extends JpaRepository<UserProduct,Long> {
    @Query("select userProduct from UserProduct userProduct where userProduct.userId=:userId order by userProduct.id")
    ArrayList<UserProduct> findByUserId(Long userId);

    @Query("select userProduct from UserProduct userProduct where userProduct.id=:id order by userProduct.id")
    Optional<UserProduct> findByRelationship(Long id);
}
