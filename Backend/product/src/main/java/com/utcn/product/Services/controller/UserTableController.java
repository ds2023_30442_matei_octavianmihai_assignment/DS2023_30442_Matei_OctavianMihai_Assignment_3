package com.utcn.product.Services.controller;

import com.utcn.product.Services.JwtTokenUtil;
import com.utcn.product.Services.Response.ApiResponse;
import com.utcn.product.Services.Response.ApiResponseBuilder;
import com.utcn.product.Services.Services.UserTableService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/usertable")
@CrossOrigin("*")
public class UserTableController {

    private final JwtTokenUtil jwtTokenUtil;
    private final UserTableService userService;

    public UserTableController(JwtTokenUtil jwtTokenUtil, UserTableService userService) {
        this.jwtTokenUtil = jwtTokenUtil;
        this.userService = userService;
    }

    @PostMapping("/add/{id}")
    public ResponseEntity<ApiResponse> AddId(@RequestBody String jwt, @PathVariable("id") Long id) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Responded", "UserController::requestMainPage");

        try {

            if(jwtTokenUtil.isLogged(jwt)){
                userService.addId(id);
            }else{
                throw new Exception("Unauthorised connection");
            }

            return new ApiResponseBuilder<>(HttpStatus.OK.value(), "Successfully")
                    .withHttpHeader(httpHeaders)
                    .build();
        } catch (Exception ex) {
            return new ApiResponseBuilder<>(HttpStatus.BAD_REQUEST.value(), ex.getMessage())
                    .withHttpHeader(httpHeaders)
                    .build();
        }
    }

    @DeleteMapping("/delete/{jwt}/{id}")
    public ResponseEntity<ApiResponse> DeleteId(@PathVariable("jwt") String jwt, @PathVariable("id") Long id) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Responded", "UserController::requestMainPage");

        try {
            if(jwtTokenUtil.isAdmin(jwt)){
                userService.removeId(id);
            }else{
                throw new Exception("Unauthorised connection");
            }

            return new ApiResponseBuilder<>(HttpStatus.OK.value(), "Successfully")
                    .withHttpHeader(httpHeaders)
                    .build();
        } catch (Exception ex) {
            return new ApiResponseBuilder<>(HttpStatus.BAD_REQUEST.value(), ex.getMessage())
                    .withHttpHeader(httpHeaders)
                    .build();
        }
    }
}
