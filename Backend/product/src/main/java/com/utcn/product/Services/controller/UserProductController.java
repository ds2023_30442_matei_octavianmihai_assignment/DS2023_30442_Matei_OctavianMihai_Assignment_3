package com.utcn.product.Services.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.utcn.product.Common.Exceptions.InvalidDataException;
import com.utcn.product.Data.dto.ProductDto;
import com.utcn.product.Data.dto.RelationshipDto;
import com.utcn.product.Data.dto.UserProductDto;
import com.utcn.product.Services.JwtTokenUtil;
import com.utcn.product.Services.Response.ApiResponse;
import com.utcn.product.Services.Response.ApiResponseBuilder;
import com.utcn.product.Services.Services.UserProductService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/api/userproduct")
public class UserProductController {

    private final JwtTokenUtil jwtTokenUtil;
    private final UserProductService userProductService;

    public UserProductController(JwtTokenUtil jwtTokenUtil, UserProductService userProductService) {
        this.jwtTokenUtil = jwtTokenUtil;
        this.userProductService = userProductService;
    }

    @GetMapping("/get_all/{jwt}")
    public ResponseEntity<ApiResponse> getAll(@PathVariable("jwt") String jwt) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Responded", "UserController::requestMainPage");

        try {
            if(jwtTokenUtil.isAdmin(jwt)) {
                ArrayList<UserProductDto> allRelationships = userProductService.getAll();
                return new ApiResponseBuilder<>(HttpStatus.OK.value(), "Successfully")
                        .withHttpHeader(httpHeaders)
                        .withData(allRelationships)
                        .build();
            }
            else{
                throw new InvalidDataException("Is not admin!");
            }
        } catch (Exception ex) {
            return new ApiResponseBuilder<>(HttpStatus.BAD_REQUEST.value(), ex.getMessage())
                    .withHttpHeader(httpHeaders)
                    .build();
        }
    }

    @GetMapping("/get_by_id/{jwt}/{id}")
    public ResponseEntity<ApiResponse> GetByUserId(@PathVariable("jwt") String jwt, @PathVariable("id") Long id) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Responded", "UserController::requestMainPage");

        try {
            if(jwtTokenUtil.isLogged(jwt)) {
                ArrayList<RelationshipDto> allRelationships = userProductService.getAllByUserId(id);
                return new ApiResponseBuilder<>(HttpStatus.OK.value(), "Successfully")
                        .withHttpHeader(httpHeaders)
                        .withData(allRelationships)
                        .build();
            }
            else{
                throw new InvalidDataException("Is not admin!");
            }
        } catch (Exception ex) {
            return new ApiResponseBuilder<>(HttpStatus.BAD_REQUEST.value(), ex.getMessage())
                    .withHttpHeader(httpHeaders)
                    .build();
        }
    }

    @PatchMapping("/update")
    public ResponseEntity<ApiResponse> updateProduct(@RequestBody ObjectNode objectNode) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Responded", "UserController::requestMainPage");
        try {
            String jwt = objectNode.get("jwt").asText();
            ObjectMapper jsonObjectMapper = new ObjectMapper();
            UserProductDto userProductDto = jsonObjectMapper.treeToValue(objectNode.get("userProductDto"), UserProductDto.class);

            if(jwtTokenUtil.isAdmin(jwt)) {
                userProductService.updateRelationship(userProductDto);
                return new ApiResponseBuilder<>(HttpStatus.OK.value(), "Successfully")
                        .withHttpHeader(httpHeaders)
                        .withData(true)
                        .build();
            }
            else{
                throw new InvalidDataException("Is not admin!");
            }
        } catch (Exception ex) {
            return new ApiResponseBuilder<>(HttpStatus.BAD_REQUEST.value(), ex.getMessage())
                    .withHttpHeader(httpHeaders)
                    .build();
        }
    }

    @DeleteMapping("/delete/{jwt}/{id}")
    public ResponseEntity<ApiResponse> deleteProduct(@PathVariable("jwt") String jwt, @PathVariable("id") Long id) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Responded", "UserController::requestMainPage");
        try {
            if(jwtTokenUtil.isAdmin(jwt)) {
                userProductService.deleteRelationship(id);
                return new ApiResponseBuilder<>(HttpStatus.OK.value(), "Successfully")
                        .withHttpHeader(httpHeaders)
                        .withData(true)
                        .build();
            }else{
                throw new InvalidDataException("Is not admin!");
            }
        } catch (Exception ex) {
            return new ApiResponseBuilder<>(HttpStatus.BAD_REQUEST.value(), ex.getMessage())
                    .withHttpHeader(httpHeaders)
                    .build();
        }
    }
}
