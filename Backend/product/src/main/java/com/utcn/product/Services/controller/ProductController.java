package com.utcn.product.Services.controller;

import com.utcn.product.Common.Exceptions.InvalidDataException;
import com.utcn.product.Data.dto.ProductDto;
import com.utcn.product.Services.JwtTokenUtil;
import com.utcn.product.Services.Response.ApiResponse;
import com.utcn.product.Services.Response.ApiResponseBuilder;
import com.utcn.product.Services.Services.ProductService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.ArrayList;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    private final JwtTokenUtil jwtTokenUtil;
    private final ProductService productService;

    public ProductController(JwtTokenUtil jwtTokenUtil, ProductService productService) {
        this.jwtTokenUtil = jwtTokenUtil;
        this.productService = productService;
    }

    @GetMapping("/get_all_products/{jwt}")
    public ResponseEntity<ApiResponse> getAllUsers(@PathVariable("jwt") String jwt) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Responded", "UserController::requestMainPage");
        try {
            if(jwtTokenUtil.isAdmin(jwt)) {
                ArrayList<ProductDto> allProducts = productService.getAll();
                return new ApiResponseBuilder<>(HttpStatus.OK.value(), "Successfully")
                        .withHttpHeader(httpHeaders)
                        .withData(allProducts)
                        .build();
            }
            else{
                throw new InvalidDataException("Is not admin!");
            }
        } catch (Exception ex) {
            return new ApiResponseBuilder<>(HttpStatus.BAD_REQUEST.value(), ex.getMessage())
                    .withHttpHeader(httpHeaders)
                    .build();
        }
    }
    @PatchMapping("/update_product")
    public ResponseEntity<ApiResponse> updateProduct(@RequestBody ObjectNode objectNode) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Responded", "UserController::requestMainPage");
        try {
            String jwt = objectNode.get("jwt").asText();
            ObjectMapper jsonObjectMapper = new ObjectMapper();
            ProductDto productDto = jsonObjectMapper.treeToValue(objectNode.get("productDto"), ProductDto.class);

            if(jwtTokenUtil.isAdmin(jwt)) {
                productService.updateUser(productDto);
                return new ApiResponseBuilder<>(HttpStatus.OK.value(), "Successfully")
                        .withHttpHeader(httpHeaders)
                        .withData(true)
                        .build();
            }
            else{
                throw new InvalidDataException("Is not admin!");
            }
        } catch (Exception ex) {
            return new ApiResponseBuilder<>(HttpStatus.BAD_REQUEST.value(), ex.getMessage())
                    .withHttpHeader(httpHeaders)
                    .build();
        }
    }

    @DeleteMapping("/delete_product/{jwt}/{id}")
    public ResponseEntity<ApiResponse> deleteProduct(@PathVariable("jwt") String jwt, @PathVariable("id") Long id) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Responded", "UserController::requestMainPage");
        try {
            if(jwtTokenUtil.isAdmin(jwt)) {
                productService.deleteProduct(id);
                return new ApiResponseBuilder<>(HttpStatus.OK.value(), "Successfully")
                        .withHttpHeader(httpHeaders)
                        .withData(true)
                        .build();
            }
            else{
                throw new InvalidDataException("Is not admin!");
            }
        } catch (Exception ex) {
            return new ApiResponseBuilder<>(HttpStatus.BAD_REQUEST.value(), ex.getMessage())
                    .withHttpHeader(httpHeaders)
                    .build();
        }
    }

}
