package com.utcn.product.Data.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Entity
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user_table", schema="product_db")
public class UserTable {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;
}
