import configparser
import pandas as pd
import time
import pika
from datetime import datetime
import json
def main():
    config = configparser.ConfigParser()
    config.read('./config_device.ini')
    default_config = config['DEFAULT']
    read_interval = int(default_config['readinterval'])
    pair_id = int(default_config['paidid'])

    print(pair_id)    

    df = pd.read_csv("./sensor.csv")
    my_values = df[df.columns[0]].tolist()

    credentials = pika.PlainCredentials('guest', 'guest')
    parameters = pika.ConnectionParameters('localhost',
                                           5672,
                                           '/',
                                           credentials)
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue='devices')

    for value in my_values:
        sending_data = {
            "timestamp": datetime.now().timestamp(),
            "pair_id": pair_id,
            "measured_value": value,
        }
        user_encode_data = json.dumps(sending_data).encode('utf-8')
        channel.basic_publish(exchange='', routing_key='devices', body=user_encode_data)
        print(sending_data)
        time.sleep(read_interval)

    connection.close()


if __name__ == '__main__':
    main()